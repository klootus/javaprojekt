package snake;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Dimension;

/**
 * Sellest klassist algab m�ng. Klass sisaldab avalehte ja tema funktsioone.
 * JFrame klassist saame graafilised funktsioonid.
 * Avalehe loomisel kasutasin Windows Builderit.
 * 
 * @author Kerttu Liis Lootus
 * @date 17/12/2015
 */

@SuppressWarnings("serial")

public class Avaleht extends JFrame {

	private JPanel contentPane;
	
	/**
	 * K�ivitab programmi.
	 * 
	 * @param args
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Avaleht frame = new Avaleht();
					frame.setVisible(true);
					}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Koostatakse avalehe raam.
	 */
	
	public Avaleht() {
		Dimension screenDim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocationByPlatform(true);
		setTitle("Snake");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds((screenDim.width - 700) / 2, (screenDim.height - 479) / 2, 700, 479);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(100, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblUssimng = new JLabel("Ussim\u00E4ng");
		lblUssimng.setHorizontalAlignment(SwingConstants.CENTER);
		lblUssimng.setBounds(285, 11, 112, 38);
		lblUssimng.setFont(new Font("Tahoma", Font.BOLD, 16));
		panel.add(lblUssimng);
		
		JLabel lblEsimeneMngija = new JLabel("Esimene m\u00E4ngija");
		lblEsimeneMngija.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblEsimeneMngija.setBounds(80, 35, 112, 27);
		panel.add(lblEsimeneMngija);
		
		JLabel lblTeineMngija = new JLabel("Teine m\u00E4ngija");
		lblTeineMngija.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblTeineMngija.setBounds(469, 35, 96, 27);
		panel.add(lblTeineMngija);
		
		JButton btnStart = new JButton("START");
		btnStart.setBounds(285, 87, 112, 37);
		
		/**
		 * Lisab nupule v��rtuse ehk kui nuppu vajutatakse, kutsutakse v�lja
		 * ussim�ng.
		 * 
		 * @param e
		 */
		
		btnStart.addActionListener(new ActionListener() { 
		public void actionPerformed(ActionEvent e) 
		{
	    btnStart.setActionCommand("Open");
	    String cmd = e.getActionCommand();
              if(cmd.equals("Open"))
			  {
			       dispose();
			       Snake.main(null);
			  }			  
		}
		});
		panel.add(btnStart);
		
		JLabel lblMnguJuhised = new JLabel("  M\u00E4ngu juhised");
		lblMnguJuhised.setBounds(273, 147, 136, 17);
		lblMnguJuhised.setFont(new Font("Tahoma", Font.BOLD, 16));
		panel.add(lblMnguJuhised);
		
		JLabel lblles = new JLabel("\u00DCLES");
		lblles.setForeground(Color.BLUE);
		lblles.setBounds(64, 260, 82, 39);
		lblles.setIcon(new ImageIcon(Avaleht.class.getResource("/ikoonid/UP.png")));
		panel.add(lblles);
		
		JLabel lblParemale = new JLabel("PAREMALE");
		lblParemale.setForeground(Color.BLUE);
		lblParemale.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblParemale.setBounds(156, 260, 112, 38);
		lblParemale.setIcon(new ImageIcon(Avaleht.class.getResource("/ikoonid/RIGHT.png")));
		panel.add(lblParemale);
		
		JLabel lblWles = new JLabel("\u00DCLES");
		lblWles.setForeground(Color.MAGENTA);
		lblWles.setBounds(453, 259, 82, 40);
		lblWles.setIcon(new ImageIcon(Avaleht.class.getResource("/ikoonid/W.png")));
		panel.add(lblWles);
		
		JLabel lblDParemale = new JLabel("PAREMALE");
		lblDParemale.setForeground(Color.MAGENTA);
		lblDParemale.setBounds(545, 259, 119, 40);
		lblDParemale.setIcon(new ImageIcon(Avaleht.class.getResource("/ikoonid/165925934 (1).png")));
		panel.add(lblDParemale);
		
		JLabel lblNewLabel_1 = new JLabel("ALLA");
		lblNewLabel_1.setForeground(Color.BLUE);
		lblNewLabel_1.setBounds(64, 311, 82, 39);
		lblNewLabel_1.setIcon(new ImageIcon(Avaleht.class.getResource("/ikoonid/DOWN.png")));
		panel.add(lblNewLabel_1);
		
		JLabel lblVasakule = new JLabel("VASAKULE");
		lblVasakule.setForeground(Color.BLUE);
		lblVasakule.setBounds(156, 310, 112, 40);
		lblVasakule.setIcon(new ImageIcon(Avaleht.class.getResource("/ikoonid/LEFT.png")));
		panel.add(lblVasakule);
		
		JLabel lblSAlla = new JLabel("ALLA");
		lblSAlla.setForeground(Color.MAGENTA);
		lblSAlla.setBounds(453, 311, 82, 39);
		lblSAlla.setIcon(new ImageIcon(Avaleht.class.getResource("/ikoonid/S.png")));
		panel.add(lblSAlla);
		
		JLabel lblAVasakule = new JLabel("VASAKULE");
		lblAVasakule.setForeground(Color.MAGENTA);
		lblAVasakule.setBounds(545, 312, 119, 37);
		lblAVasakule.setIcon(new ImageIcon(Avaleht.class.getResource("/ikoonid/165925934.png")));
		panel.add(lblAVasakule);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Avaleht.class.getResource("/ikoonid/key_space_bar_small.png")));
		lblNewLabel.setBounds(278, 259, 127, 40);
		panel.add(lblNewLabel);
		
		JLabel lblPausUus = new JLabel("PAUS / UUS M\u00C4NG");
		lblPausUus.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblPausUus.setHorizontalAlignment(SwingConstants.CENTER);
		lblPausUus.setBounds(273, 317, 136, 27);
		panel.add(lblPausUus);
		
		JLabel lblNewLabel_2 = new JLabel("M\u00E4ng kestab 60 sekundit. V\u00F5idab see, kes selle aja jooksul rohkem marju s\u00F6\u00F6b.");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(64, 194, 525, 27);
		panel.add(lblNewLabel_2);
		
		JLabel lblEdu = new JLabel("EDU!");
		lblEdu.setForeground(Color.RED);
		lblEdu.setHorizontalAlignment(SwingConstants.CENTER);
		lblEdu.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblEdu.setBounds(285, 367, 112, 38);
		panel.add(lblEdu);
		
		JLabel lblSinineUss = new JLabel("SININE USS");
		lblSinineUss.setForeground(Color.BLUE);
		lblSinineUss.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblSinineUss.setHorizontalAlignment(SwingConstants.CENTER);
		lblSinineUss.setBounds(80, 60, 112, 20);
		panel.add(lblSinineUss);
		
		JLabel lblRoosaUss = new JLabel("ROOSA USS");
		lblRoosaUss.setForeground(Color.MAGENTA);
		lblRoosaUss.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblRoosaUss.setHorizontalAlignment(SwingConstants.CENTER);
		lblRoosaUss.setBounds(469, 63, 96, 17);
		panel.add(lblRoosaUss);
		
		JLabel lblPsOnMarju = new JLabel("PS! Leidub marju, mis pikendavad aega.");
		lblPsOnMarju.setHorizontalAlignment(SwingConstants.CENTER);
		lblPsOnMarju.setBounds(80, 221, 485, 14);
		panel.add(lblPsOnMarju);
	}
}
