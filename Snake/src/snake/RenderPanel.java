package snake;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import javax.swing.JPanel;

/**
 * Ussim�ngu graafiline pool.
 * JPanel klassist saame graafilised funktsioonid.
 * 
 */

@SuppressWarnings("serial")
public class RenderPanel extends JPanel
{
	public RenderPanel() {
		
	}
	
	/**
	 * Antud meetod sisaldab ussi m�ngu graafilist poolt.
	 *
	 *@param g
	 */
	
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		Snake snake = Snake.uss;
		
		//M��rame tausta
		
	    g.setColor(Color.BLACK);
		
		g.fillRect(0, 0, 800, 700);
		
		//Esimene ussi v�limus.
		
		g.setColor(Color.BLUE);

		for (Point point : snake.ussiOsad1)
		{
			g.fillRect(point.x * Snake.SKAALA, point.y * Snake.SKAALA, Snake.SKAALA, Snake.SKAALA );
		}
		
		g.setColor(Color.ORANGE);
		
		g.fillRect(snake.pea1.x * Snake.SKAALA, snake.pea1.y * Snake.SKAALA, Snake.SKAALA, Snake.SKAALA);
		
		//Teise ussi v�limus.
		
		g.setColor(Color.MAGENTA);
		
		for (Point point2 : snake.ussiOsad2)
		{
			g.fillRect(point2.x * Snake.SKAALA, point2.y * Snake.SKAALA, Snake.SKAALA, Snake.SKAALA);
		}
		
		g.setColor(Color.GREEN);
		
		g.fillRect(snake.pea2.x * Snake.SKAALA, snake.pea2.y * Snake.SKAALA, Snake.SKAALA, Snake.SKAALA);
		
		//Tavaline mari nr1
		
		g.setColor(Color.RED);
		
		g.fillOval(snake.mari1.x * Snake.SKAALA, snake.mari1.y * Snake.SKAALA, Snake.SKAALA, Snake.SKAALA);
		
		//Tavaline mari nr2
		
        g.setColor(Color.RED);
        
		g.fillOval(snake.mari2.x * Snake.SKAALA, snake.mari2.y * Snake.SKAALA, Snake.SKAALA, Snake.SKAALA);
		
		//Mari, mis annab aega juurde ja ilmub 25-15 sekundi vahel.
		
		g.setColor(Color.lightGray);
		
		if (snake.aeg < 25*20 && snake.aeg >15*20)
		{
		 g.fillOval(snake.mari3.x * Snake.SKAALA, snake.mari3.y * Snake.SKAALA, Snake.SKAALA, Snake.SKAALA);
		}

		//�lemine tekst.
				
		String string = "Sinise ussi skoor: " + snake.skoor1;
		
		g.setColor(Color.CYAN);
		
		g.drawString(string, 20, 20);
		
		g.setColor(Color.white);
		
		string = "Aega j��nud: " + snake.aeg / 20;
		
		g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), 20);
		
		string = "Roosa ussi skoor: " + snake.skoor2;
		
		g.setColor(Color.MAGENTA);
		
		g.drawString(string, 650, 20);
		
		//Kui m�ng on l�bi.

		string = "M�NG L�BI!!!";
		
		String rida= "Uuesti m�ngimiseks vajuta t�hikut ehk SPACE.";
		
		g.setColor(Color.WHITE);

		if (snake.l2bi)
		{
			g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), (int) snake.dim.getHeight() / 4);
			
			g.drawString(rida,(int) (getWidth() / 2 - rida.length() * 2.5f), (int) snake.dim.getHeight() / 2);
		}
		
		//Kui sinine v�idab.
		
		string = "SININE V�ITIS " + (snake.skoor1-snake.skoor2) + " PUNKTIGA";
		
		g.setColor(Color.BLUE);
		
		if (snake.l2bi && snake.skoor1 > snake.skoor2)
		{
			g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), (int) snake.dim.getHeight() / 3);
		}
		
		//Kui roosa v�idab.
		
		string = "ROOSA V�ITIS " + (snake.skoor2-snake.skoor1) + " PUNKTIGA";
		
		g.setColor(Color.MAGENTA);
		
		if (snake.l2bi && snake.skoor2 > snake.skoor1)
		{
			g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), (int) snake.dim.getHeight() / 3);
		}
		
		//Kui m�ng on pausil.
		
		g.setColor(Color.WHITE);

		string = "Paused!";

		if (snake.pausil && !snake.l2bi)
		{
			g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), (int) snake.dim.getHeight() / 4);
		}
		
	}
}

