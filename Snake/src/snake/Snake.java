package snake;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.Timer;

/**
 * Klass, mis koosneb m�ngu loogikast.
 */

public class Snake implements ActionListener, KeyListener
{
	public static Snake uss;

	public JFrame raam;
	
	public RenderPanel renderPanel ,renderPanel2;

	public Timer timer = new Timer(20, this);

	public ArrayList<Point> ussiOsad1 = new ArrayList<Point>();
	
	public ArrayList<Point> ussiOsad2 = new ArrayList<Point>();

	public static final int UP = 0, DOWN = 1, LEFT = 2, RIGHT = 3, SKAALA = 10, UP2 = 0, DOWN2 = 1, LEFT2 = 2, RIGHT2 = 3;
	
    public int ticks = 0, suund1 = DOWN, skoor1, sabaSuurus1 = 14, sabaSuurus2, aeg, skoor2, suund2 = DOWN2;

	public Point pea1, pea2, mari1, mari2, mari3;

	public Random random;

	public boolean l2bi = false, pausil;

	public Dimension dim;
	
	/**
	 * Luuakse m�ngu raamistik.
	 */
	
	public Snake()
	{
		dim = Toolkit.getDefaultToolkit().getScreenSize();
		raam = new JFrame("Snake");
		raam.setVisible(true);
		raam.setSize(805, 700);
		raam.setResizable(false);
		raam.setLocation(dim.width / 2 - raam.getWidth() / 2, dim.height / 2 - raam.getHeight() / 2);
		raam.add(renderPanel2 = new RenderPanel());
		raam.add(renderPanel = new RenderPanel());
	    raam.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		raam.addKeyListener(this);
		
		startGame();
	}
	
    /**
     * Antakse m�ngu tingumused ja v��rtused ning k�ivitatakse timer.
     */
	
	public void startGame()
	{
		l2bi = false;
		pausil = false;
		aeg = 1200;
		skoor1 = 0;
		skoor2 = 0;
		sabaSuurus1 = 14;
		sabaSuurus2 = 14;
		ticks = 0;
		suund1 = DOWN;
		suund2 = DOWN2;
		pea1 = new Point(0, -1);
		pea2 = new Point(20, -1);
		random = new Random();
		ussiOsad1.clear();
		ussiOsad2.clear();
		mari1 = new Point(random.nextInt(79), random.nextInt(66));
		mari2 = new Point(random.nextInt(79), random.nextInt(66));
		mari3 = new Point(random.nextInt(79), random.nextInt(66));
			
		timer.start();
	}
	
	/**
	 * Antud meetodis saadakse kasutaja k�skudele t�hendused.
	 * Lisaks sisaldab tingimusi, mis l�petavad m�ngu v�i kui s��dakse marju.
	 * 
	 * @param arg0
	 * 
	 */
	
	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		if (aeg == 0)
		{
			l2bi = true;
		}

		//Esimese ussi tingimused.
		
		renderPanel.repaint();
		ticks--;

		if (ticks % 2 == 0 && pea1 != null && !l2bi && !pausil)
		{
			aeg--;

			ussiOsad1.add(new Point(pea1.x, pea1.y));

			if (suund1 == UP)
			{
				if (pea1.y - 1 >= 0)
				{
					pea1 = new Point(pea1.x, pea1.y - 1);
				}
				else
				{
					pea1 = new Point(pea1.x, 66 - pea1.y);
				}
			}

			if (suund1 == DOWN)
			{
				if (pea1.y + 1 < 67 )
				{
					pea1 = new Point(pea1.x, pea1.y + 1);
				}
				else
				{
					pea1 = new Point(pea1.x, 0);
				}
			}

			if (suund1 == LEFT)
			{
				if (pea1.x - 1 >= 0 )
				{
					pea1 = new Point(pea1.x - 1, pea1.y);
				}
				else
				{
					pea1 = new Point(79 - pea1.x, pea1.y);
				}
			}

			if (suund1 == RIGHT)
			{
				if (pea1.x + 1 < 80 )
				{
					pea1 = new Point(pea1.x + 1, pea1.y);
				}
				else
				{
					pea1 = new Point(0, pea1.y);
				}
			}

			if (ussiOsad1.size() > sabaSuurus1)
			{
				ussiOsad1.remove(0);

			}

			if (mari1 != null)
			{
				if (pea1.equals(mari1))
				{
					skoor1 += 10;
					sabaSuurus1++;
					mari1.setLocation(random.nextInt(79), random.nextInt(66));
				}
			}
			
			if (mari2 != null)
			{
				if (pea1.equals(mari2))
				{
					skoor1 += 10;
					sabaSuurus1++;
					mari2.setLocation(random.nextInt(79), random.nextInt(66));
				}
			}
			
			if (mari3 != null)
			{
				if (pea1.equals(mari3))
				{
					aeg += 30*20;
					skoor1 += 20;
					sabaSuurus1 += 4;
					mari3.setLocation(random.nextInt(79), random.nextInt(66));
				}
			}
						
		//TEINE USS
			
		renderPanel2.repaint();
						
		if (ticks % 2 == 0 && pea2 != null && !l2bi && !pausil)
		{			
                ussiOsad2.add(new Point(pea2.x, pea2.y));

			if (suund2 == UP2)
			{
					if (pea2.y - 1 >= 0 )
					{
						pea2 = new Point(pea2.x, pea2.y - 1);
					}
					else
					{
						pea2 = new Point(pea2.x, 66 - pea2.y);

					}
			}

			if (suund2 == DOWN2)
			{
					if (pea2.y + 1 < 67)
					{
						pea2 = new Point(pea2.x, pea2.y + 1);
					}
					else
					{
						pea2 = new Point(pea2.x, 0);
					}
				}

			if (suund2 == LEFT2)
			{
					if (pea2.x - 1 >= 0)
					{
						pea2 = new Point(pea2.x - 1, pea2.y);
					}
					else
					{
						pea2 = new Point(79 -pea2.x, pea2.y);
					}
			}

			if (suund2 == RIGHT2)
			{
					if (pea2.x + 1 < 80 )
					{
						pea2 = new Point(pea2.x + 1, pea2.y);
					}
					else
					{
						pea2 = new Point(0, pea2.y);
					}
			}

			if (ussiOsad2.size() > sabaSuurus2)
			{
					ussiOsad2.remove(0);
			}

			if (mari1 != null)
			{
					if (pea2.equals(mari1))
					{
						skoor2 += 10;
						sabaSuurus2++;
						mari1.setLocation(random.nextInt(79), random.nextInt(66));
					}
			}
			if (mari2 != null)
			{
					if (pea2.equals(mari2))
					{
						skoor2 += 10;
						sabaSuurus2++;
						mari2.setLocation(random.nextInt(79), random.nextInt(66));
					}
			}
			
			if (mari3 != null)
			{
					if (pea2.equals(mari3))
					{
						aeg += 30*20;
						skoor2 += 20;
						sabaSuurus2 += 4;
						mari3.setLocation(random.nextInt(79), random.nextInt(66));
					}
			}
		}	
		}
	}
	
	/**
	 * Antud meetod kutsub v�lja Snake meetodi, mis alustab m�ngu.
	 * 
	 * @param args
	 */

	public static void main(String[] args)
	{
		uss = new Snake();		
	}
	
	/**
	 * Antud meetod s�testab kasutaja klahvivajutustele t�hendused.
	 * 
	 * @param e
	 */

	@Override
	public void keyPressed(KeyEvent e)
	{
		int i = e.getKeyCode();
		
	/**
	 * Esimese ussi liikumisk�sud.
	 */

	    if (( i == KeyEvent.VK_LEFT) && suund1 != RIGHT)
		{
			suund1 = LEFT;
		}

		if (( i == KeyEvent.VK_RIGHT) && suund1 != LEFT)
		{
			suund1 = RIGHT;
		}

		if (( i == KeyEvent.VK_UP) && suund1 != DOWN)
		{
			suund1 = UP;
		}

		if (( i == KeyEvent.VK_DOWN) && suund1 != UP)
		{
			suund1 = DOWN;
			
		}
		
	/**
	 * Teise ussi liikumisk�sud.
	 */
		
	    if ((i == KeyEvent.VK_A ) && suund2 != RIGHT2)
		{
			suund2 = LEFT2;
		}

		if ((i == KeyEvent.VK_D ) && suund2 != LEFT2)
		{
			suund2 = RIGHT2;
		}

		if ((i == KeyEvent.VK_W ) && suund2 != DOWN2)
		{
			suund2 = UP2;
		}

		if ((i == KeyEvent.VK_S ) && suund2 != UP2)
		{
			suund2 = DOWN2;
		}
		
		/**
		 * T�hik ehk alustab uut m�ngu v�i paneb pausile.
		 */

		if (i == KeyEvent.VK_SPACE)
		{
			if (l2bi)
			{
				startGame();
			}
			else
			{
				pausil = !pausil;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
	}

	@Override
	public void keyTyped(KeyEvent e)
	{ 
	}
}